export interface Employee {
        id?:number;
        salon_id?:number;
        first_name?:string;
        username?:string;
        password?:string;
        role_id?:number;
        payrate_type_id?:number;
        sharing_rate?:number;
        check_rate?:number;
        bao_rate?:number;
        start_date?:string;
        status?:number;
        pin?:string;
}