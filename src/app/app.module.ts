import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { Http, HttpModule,RequestOptions } from "@angular/http";
import { ModalModule } from "ngx-bootstrap";
import { NgDatepickerModule } from "ng2-datepicker";
import { AuthHttp, AuthConfig } from "angular2-jwt";

import * as $ from "jquery";
import * as bootstrap from "bootstrap";

import { AuthService } from "./services/auth.service";
import { EmployeeService } from "./services/employee.service";
import { RequestService } from "./services/request.service";
import { AuthGuard } from "./guards/auth.guard";

import { AppComponent } from "./app.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { LoginComponent } from "./components/login/login.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { LayoutComponent } from "./components/layout/layout.component";
import { ServiceComponent } from "./components/service/service.component";
import { EmployeeComponent } from "./components/employee/employee.component";
import { HomeComponent } from "./components/home/home.component";
import { NewEmployeeModalComponent } from "./components/new-employee-modal/new-employee-modal.component";

// Create Routes

const appRoutes: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
    canActivate: [AuthGuard]
  },
  {
    path: "home",
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full",
        canActivate: [AuthGuard]
      },
      {
        path: "dashboard",
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "services",
        component: ServiceComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "employees",
        component: EmployeeComponent,
        canActivate: [AuthGuard]
      }
    ]
  },

  { path: "login", component: LoginComponent }
];
export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(
    new AuthConfig({
      tokenName: "token",
      tokenGetter: () => localStorage.getItem("access_token"),
      globalHeaders: [{ "Content-Type": "application/json" }]
    }),
    http,
    options
  );
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    LoginComponent,
    DashboardComponent,
    LayoutComponent,
    ServiceComponent,
    EmployeeComponent,
    HomeComponent,
    NewEmployeeModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ModalModule.forRoot(),
    NgDatepickerModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
    AuthService,
    AuthGuard,
    EmployeeService,
    RequestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
