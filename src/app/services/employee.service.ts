import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from "../../environments/environment.dev";
import { Employee } from "../models/Employee";
import { RequestService } from "../services/request.service";
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class EmployeeService {

  private employeeUrl = environment.apiUrl + "/employees/";

  constructor(private _request: RequestService, public authHttp: AuthHttp ) { }

  addEmployee(employee: Employee)
  {
    return this.authHttp.post(this.employeeUrl,employee)
    .subscribe(
      data => console.log(data),
      err => console.log(err),
      () => console.log('Request Complete')
    );
  }

  getEmployee(salonId)
  {
    return this.authHttp.get(this.employeeUrl + salonId)
    .subscribe(
      data => console.log(data),
      err => console.log(err),
      () => console.log('Request Complete')
    );

  }
}