import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Http } from "@angular/http";
import { HttpHeaders } from "@angular/common/http";
import { RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import { tokenNotExpired } from 'angular2-jwt';
import { JwtHelper } from 'angular2-jwt';

import { Login } from "../models/Login";
import { environment } from "../../environments/environment.dev";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class AuthService {
  private authUrl = environment.apiUrl + "/auth/login/";
  public token: string;
  jwtHelper: JwtHelper = new JwtHelper();
  
  constructor(
    private _http: Http
  ) {
    this.token = localStorage.getItem("access_token");
    
  }

  login(username: string, password: string) {
    let body = { username: username, password: password };

    return this._http.post(this.authUrl, body).map(res => {
      let response = res.json();
      if (this.token === null || !tokenNotExpired('access_token')) {
        localStorage.setItem("access_token", response.token);
        this.token = response.token;
      } else {
        console.log(response.message);
      }
    });
  }

  isAuthenticated(): boolean {
    return tokenNotExpired('access_token');
  }

  getDecodedToken() {
    
    return  this.jwtHelper.decodeToken(this.token);
    
  }

  logout() {
    localStorage.removeItem("access_token");
  }
}
