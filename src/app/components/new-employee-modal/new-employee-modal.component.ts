import { Component, OnInit } from "@angular/core";
import { Role } from "../../models/Role";
import { PayrateType } from "../../models/PayrateType";
import { Employee } from "../../models/Employee";
import { AuthService } from "../../services/auth.service";
import { EmployeeService } from "../../services/employee.service";
@Component({
  selector: "app-new-employee-modal",
  templateUrl: "./new-employee-modal.component.html",
  styleUrls: ["./new-employee-modal.component.css"]
})
export class NewEmployeeModalComponent implements OnInit {

  private roleValue: number;
  private payrateType: number;
  
  roles: Role[] = [
    {
      id: 1,
      name: "Owner",
      display_name: "Owner"
    },
    {
      id: 2,
      name: "Manager",
      display_name: "Manager"
    },
    {
      id: 3,
      name: "technician-khong-bao",
      display_name: "Technician Khong Bao Luong"
    },
    {
      id: 4,
      name: "technician-bao",
      display_name: "Technician Bao Luong"
    }
  ];

  payrateTypes: PayrateType[] = [
    {
      id : 2,
      name: "hr",
      display_name: "Hour"
    },
    {
      id : 3,
      name: "day",
      display_name: "Day"
    },
    {
      id : 4,
      name: "week",
      display_name: "Week"
    },
    {
      id : 5,
      name: "month",
      display_name: "Month"
    }
  ]

  employee: Employee = {
    id: null,
    salon_id: null,
    first_name: "",
    username: "",
    password: "",
    role_id: null,
    payrate_type_id: null,
    sharing_rate: 0,
    check_rate: 0,
    bao_rate: 0,
    start_date: new Date().toString(),
    status: 1,
    pin: ""
  }
    ;

    private salon_id: number;
    private decodeToken: string;

  constructor(private authService: AuthService, private employeeService: EmployeeService) {
    this.roleValue = 0;
    this.decodeToken = this.authService.getDecodedToken();
    this.employee.salon_id = this.decodeToken['emp_info']['salon_id'];
  }

  ngOnInit() {
  }

  onChange(value){
    this.roleValue = value;
  }

  onChangePayrate(value){
    this.payrateType = value;
  }
  onSubmit({ value, valid }: { value: Employee; valid: boolean })
  {
    this.employee.role_id = value.role_id;
    this.employeeService.addEmployee(this.employee);
    $(".addItem-modal").modal("hide");
  }

}
