import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  isAuthenticated: boolean;
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.isAuthenticated = true;
    } else {
      this.isAuthenticated = false;
    }
  }

  onLogoutClick() {
    this.authService.logout();
    this.router.navigate(["/login"]);
  }
}
