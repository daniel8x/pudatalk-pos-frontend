import { Component, OnInit } from "@angular/core";
import { NewEmployeeModalComponent } from "../new-employee-modal/new-employee-modal.component";
import { EmployeeService } from "../../services/employee.service";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-employee",
  templateUrl: "./employee.component.html",
  styleUrls: ["./employee.component.css"]
})
export class EmployeeComponent implements OnInit {
  private newEmployeeModal: NewEmployeeModalComponent;
  private decodeToken: string;
  constructor(private authService: AuthService, private employeeService: EmployeeService) {}

  ngOnInit() {
    this.decodeToken = this.authService.getDecodedToken();
    this.employeeService.getEmployee(this.decodeToken['emp_info']['salon_id']);
  }

  addEmployeeModal() {
    $(".addItem-modal").modal("show");
  }
}
