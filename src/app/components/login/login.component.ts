import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Login } from "../../models/Login";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  
  message: string;
  showError: boolean;

  login: Login = {
    username: "",
    password: ""
  };

  constructor(private authService: AuthService, private router: Router) {
    this.message = "";
    this.showError = false;
  }

  ngOnInit() {}

  onSubmit({ value, valid }: { value: Login; valid: boolean }) {
    this.authService.login(this.login.username, this.login.password).subscribe(
      data => {
        this.router.navigate(["/"]);
      },
      error => {
        this.showError = true;

        setTimeout(() => {
          this.showError = false;
        }, 5000);

        this.message = error.json().message;
        this.router.navigate(["/login"]);
      }
    );
  }
}
